resource "aws_instance" "web" {
    ami = "ami-03130878b60947df3"
    instance_type = "t3.micro"
    key_name = "patricia-gitlab-poc"
    
    tags = {
        Name = "patricia-gitlab-terraform-code-test"
    } 
}

resource "aws_instance" "ubuntu" {
    ami = "ami-0a741b782c2c8632d"
    instance_type = "t3.micro"
    key_name = "patricia-gitlab-poc"

    tags = {
        Name = "HelloWorld"
    }
}

resource "aws_instance" "ubuntu2" {
    ami = "ami-031b673f443c2172c"
    instance_type = "t3.micro"
    key_name = "patricia-gitlab-poc"

    tags = {
        Name = "Test-ec2-add"
    }
}




